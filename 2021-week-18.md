# Week 18 | May 4 - May 7 👁💗🤝✨ 
#### Main focus 

- Analyse solution validation for pipeline graph
- Design exploration

#### Milestone 

[13.12](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/16#scope-of-work-for-testing)

## Tasks

#### Research and Design

- [x] Create an issue to document pipeline graph in PJs, add info from Daniel's issue. [See this comment](https://gitlab.com/gitlab-org/gitlab/-/issues/325860#note_538404045)
- [x] Analyse solution validation for the job dependencies view

#### Other

- [x] Share the results of the UX Collaboration survey to set a benchmark in an MR
- [x] Iterate on the process MR Sam suggested
- [x] 14.0 planning

#### Focus Friday

🌈🌻 [Personal Development Goals Q2](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/40) 🌻🌈 [UX department OKRs Q2](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1603) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈

- [x] Finish going over the materials for [interview training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1068#note_538940577)
- [x] Provide feedback in UX co-working
- [x] Reflect on the week + next week planning

</p>
</details>

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Short but productive week! 
- Very successful solution validation for the job dependencies view, gathered lots of feedback in a short period of time.
- Finally completed the interview training materials!

#### What I learnt 

- Learnt a lot about our hiring process.

#### What didn't go so well? Why?

- I fell behind on issue pings after an extra day off, next time I need to prioritise MR pings on the first day after OOO. I ended up getting to them only on Wednesday which meant a couple days waiting for some folks.

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Design: [Add dropdown affordance to the pipeline mini graph](https://gitlab.com/gitlab-org/gitlab/-/issues/292002#note_552752899) and [Make pipeline mini graph representation more scalable](https://gitlab.com/gitlab-org/gitlab/-/issues/327900)
  - [ ] Create a proposal
  - [ ] Request feedback
- [ ] Gather feedback on and iterate on the [visual builder issue](https://gitlab.com/gitlab-org/gitlab/-/issues/327210) 
- [ ] [Review PA direction, do additional research and make notes](https://gitlab.com/dfosco/dfosco/-/blob/main/deployments-direction.md)
- [ ] Make a small MR for an iterative change to the PA handbook [based on my intitial proposal](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/79400#note_565473815)
- [ ] Fill out the [Senior Product Designer justification doc](https://docs.google.com/document/d/1d0Tk6usyOJNkNx7HmbtwmA0pNgw2k0Gr6yxRX6mE-fE/edit?usp=sharing)
- [ ] Q2 OKRs and Q2 personal goals first steps
- [ ] Listen to The Bad Conference recordings and take notes
- [ ] [Organise conference notes](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/39) and worked on a merged version with Veethika (may carry over to next week)

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] [Try RICE framework](https://www.intercom.com/blog/rice-simple-prioritization-for-product-managers/)
- [ ] Competitor analysis for Appsmith.com
- [ ] Create a new page for accessibility resources in Pajamas accessibility section
- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Run a competitor analysis for PA ([example](https://gitlab.com/gitlab-org/gitlab/-/issues/326285))
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Read https://medium.com/better-programming/circleci-vs-gitlab-choose-the-right-ci-cd-tool-f920ce90ea09 
- [ ] Watch [Figma workshop](https://www.youtube.com/watch?v=sCRYJdKoaxE&mkt_tok=eyJpIjoiTVRsaE9XUXdOamRrTnpObSIsInQiOiJqU3FBdEhiQnRHSFZIaUdudVd6WjJTSVZcL0ZSY2hXa1R5TzBcL1lUNER4RER4bDJUQmpYZDMwamNHZHhQd1hTWk9nUWEzQ1VvVTh1UGxLaUtmWFluXC95QmlHOTR3WEwzVjNCVTU0ektjbEpCUzFqYWwxTEV0blZBc0t6d3E3ejVNaiJ9) 
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 
- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/

</p>
</details>
