# Week 15 | April 19 - April 22 👁💗🤝✨

#### Main focus 

- 15.0 design issues

#### Milestone 

- [15.0](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/44#scope-of-work-for-ux)

## Tasks

#### Research

#### Design

- [x] Refine the user flows for the CI/CD Catalog MVC
- [x] [Design: Add loading to the pipeline graph](https://gitlab.com/gitlab-org/gitlab/-/issues/351831)

#### Pajamas

#### Other

- [x] 14.10 [UX](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/52) and PA retro
- [ ] Create an issue for the PA JTBD mapping (see [Veethika's issue as an example](https://gitlab.com/gitlab-org/ux-research/-/issues/1900))

#### Focus Friday 🔮 

🌻🌈 [CI/CD UX OKRs Q1](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/50) 🌻🌈 [FY23 Q1 & Q2](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [x] [Refine the illustrations audit epic](https://gitlab.slack.com/archives/D010YE5CFBL/p1649944348463559) (audit [epic](https://gitlab.com/groups/gitlab-org/-/epics/7847))
- [ ] Refine the [Pipeline Authoring UX page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100082) and request a review from Rayana ([Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and [PE](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/) pages for structure)
- [ ] Think about what I should strategically share in the next UX showcase
- [x] Pajamas traineeship checkin
- [ ] Provide feedback in UX co-working 
- [x] Reflect on and plan next week

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Raw variables: Create a screener for the Usertesting.com test
- [ ] Raw variables: Create prototypes and set up a test on Usertesting.com
- [ ] Review the [open questions for the CI catalog](https://gitlab.com/gitlab-org/gitlab/-/issues/352347) and the [MVC user flows](https://www.figma.com/file/Eyhsf25hoig4OFAoqJs9i4/%F0%9F%92%9CCI%2FCD-Catalog?node-id=1442%3A7229) with Fabio and Avielle
- [ ] Refine the [CI Catalog MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/359047) scope with Dov
- [ ] Schedule a team discussion to gather feedback on the CI Catalog MVC from the whole PA team for next week
- [ ] Refine the user flows for the [CI Catalog MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/359047), add product screenshots and page/feature locations and visionary mock-ups where possible
- [ ] Record a video walkthrough for our vision for the CI/CD catalog and the MVC user flows to gather feedback from the team prior to the call, and the GitLab team at large, especially the team potential for dogfooding. Share the walkthrough and gather feedback in the MVC issue in threads. 
- [ ] Collab on the Secrets research analysis with Erika
- [ ] Engage the UX team in the [help drawer discussions](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1238#note_920665531)

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
