# Week 38 | September 20 - September 23 👁💗🤝✨ (4 days)

#### Main focus 

- Solution validation planning

#### Milestone 

- [14.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/26#scope-of-work-for-ux)

## Tasks

#### Research and Design

- [x] Finalise the proposal for [retrying all DSPs from the trigger job.](https://gitlab.com/gitlab-org/gitlab/-/issues/32559/) 
- [x] [Create a plan and prototype for Solution validation: Pipeline creation simulation](https://gitlab.com/gitlab-org/gitlab/-/issues/337282/)
- [-] Create a plan and a testing environment for Solution Validation: Pipeline Editor usability testing
- [x] Move forward the discussions around the [mini pipeline graph issue](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/946)
- [x] Collaborate on the job conceptual model with Pedro, ping Jeremy for feedback too


#### Other

- [x] Provide feedback in UX co-working
 - [x] Reflect and plan next week + next milestone 
 - [x] Draft up focus points for the Q4 career dev issues based on the [360/performance Review](https://docs.google.com/document/d/1KF9WjTbc1F_Pg-ZPVrEzW1sS12THkRUcOmsU7GDuay0/edit#) and [Culture Amp focus areas](https://gitlab.cultureamp.com/effectiveness/surveys/60d12446afb34f002b69aeb4/processes/61048a96ae6511868581dd4d/feedback_review/new_take_action#)
 - [x] Select top things to focus on before the Nov review (what do I need to improve the most?)

#### Focus Friday 🔮 FFDAY

🌈🌻 [Personal Development Goals Q3](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46) 🌻🌈 [Q4](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/50) 🌻🌈 [UX department OKRs Q3](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1660) 🌈🌻 [Verify Hive OKRs Q3](https://gitlab.com/groups/gitlab-org/-/epics/6313) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈 [Mentorship](https://gitlab.com/emchang/open-notebook/-/issues/10)🌈🌻

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>

- [ ] Create a plan and a testing environment for Solution Validation: Pipeline Editor usability testing
- [ ] Set up an FE call for when Sarah comes back. Discuss the [mini pipeline graph component updates](https://gitlab.com/gitlab-org/gitlab/-/issues/340827), [manual job permissions issue](https://gitlab.com/gitlab-org/gitlab/-/issues/339922#note_673800253)

<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Reflect back on the [async collab process with the Growth team](https://gitlab.com/gitlab-org/gitlab/-/issues/332248#note_620427152)
- [ ] Reflect back on the mentorship program
- [ ] [Review PA direction, do additional research and make notes](https://gitlab.com/dfosco/dfosco/-/blob/main/deployments-direction.md)
- [ ] [Try RICE framework](https://www.intercom.com/blog/rice-simple-prioritization-for-product-managers/)
- [ ] Create a new page for accessibility resources in Pajamas accessibility section
- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Read https://medium.com/better-programming/circleci-vs-gitlab-choose-the-right-ci-cd-tool-f920ce90ea09 
- [ ] Watch [Figma workshop](https://www.youtube.com/watch?v=sCRYJdKoaxE&mkt_tok=eyJpIjoiTVRsaE9XUXdOamRrTnpObSIsInQiOiJqU3FBdEhiQnRHSFZIaUdudVd6WjJTSVZcL0ZSY2hXa1R5TzBcL1lUNER4RER4bDJUQmpYZDMwamNHZHhQd1hTWk9nUWEzQ1VvVTh1UGxLaUtmWFluXC95QmlHOTR3WEwzVjNCVTU0ektjbEpCUzFqYWwxTEV0blZBc0t6d3E3ejVNaiJ9) 
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 
- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/

</p>
</details>
