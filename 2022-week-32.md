# Week 32 | Aug 8 - Aug 12  👁💗🤝✨

#### Main focus 

#### Milestone 

- [15.3](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/67)

## Tasks

#### Research

#### Design

- [x] Get feedback on the CI/CD catalog UI from the UX dept
- [ ] Finalise [Design: Show an error in the pipeline simulation when configuration only contains .pre and .post jobs](https://gitlab.com/gitlab-org/gitlab/-/issues/219416)
- [ ] Finalise [Trigger job stuck in pending state even when the downstream pipeline is already running](https://gitlab.com/gitlab-org/gitlab/-/issues/363311)
- [x] Finish the CI/CD catalog designs https://gitlab.com/gitlab-org/gitlab/-/issues/359047#note_1045127315+
- [x] Ping the engineers in threads or on designs for a final discussion
- [x] Finalise https://gitlab.com/gitlab-org/gitlab/-/issues/30098+
- [x] [Start secrets JTBD validation analysis](https://gitlab.com/gitlab-org/ux-research/-/issues/1981)

#### Pajamas

- [ ] Finish the Tanukis update

#### Other

- [x] Do the [MECC training](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2058)
- [ ] Share the [updated guidelines for drawer usage in GitLab](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/3007/diffs)
- [ ] Gather ideas for Beautifying UI in Figma and post to [beautifying UI issue](https://gitlab.com/gitlab-org/gitlab/-/issues/370364)
- [x] Share the course justification with Rayana

#### Focus Friday 🔮 

🌻🌈 [FY23 Q2 CI/CD UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978#fy23-q2-okrs-for-cicd-ux) 🌻🌈 [FY23 Q2-Q3 Career Development](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [ ] Pajamas traineeship checkin ([see here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835#note_929756938))
- [ ] Provide feedback in UX co-working
- [x] Reflect on and plan next week
- [ ] Refine the [Q2-3 Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) based on the feedback from Rayana and scope down ruthlessly

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Completed the MECC training!
- Talked to the engineer I'll be pairing up with for Beautifying UI, we already have some ideas.
- Got a good book recommendation from Rayana.

#### What I learnt 

- Had a very productive discussion with Grzegorz and Fabio, learnt a lot about CI templates.

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Collaborate with Erika on the JTBDs in the sync 
- [ ] Make an MR to update the secrets management JTBDs that we will use to validate our maturity
- [ ] Refine the CI/CD catalog proposal based on the [latest discussions](https://gitlab.com/gitlab-org/gitlab/-/issues/359047#note_1057162893) and catalog UI feedback
- [ ] Create a video + issue update of the pipeline components research insights, MVC direction, next steps and future improvements.
- [ ] Share in the UX, CI-CD UX, PA, Verify channels
- [ ] Provide 360 feedback

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
