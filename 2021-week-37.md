# Week 37 | September 13 - September 17 👁💗🤝✨ 

#### Main focus 

- Wrapping up 14.3
- Finishing up Pajamas contributions
- Planning next milestone

#### Milestone 

- [14.3](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/25#scope-of-work-for-ux)
- [14.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/26#scope-of-work-for-ux)

## Tasks

#### Research and Design

- [x] [Design: Empty states exploration/ validation](https://gitlab.com/gitlab-org/gitlab/-/issues/338198)
- [x] Add the mini pipeline graph announcement to UX weekly and share in UX chanel
- [x] [Fix the commit names in the MR](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2529#note_676096510)
- [x] Hold the discussions around the [mini pipeline graph issue](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/946)

#### Other
 
- [x] Ping Katie on all issues and MRs
- [x] Record a [video for editing commits in gitpod](https://www.youtube.com/watch?v=gnchZBVQZg8)

#### Focus Friday 🔮 

🌈🌻 [Personal Development Goals Q3](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46) 🌻🌈 [UX department OKRs Q3](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1660) 🌈🌻 [Verify Hive OKRs Q3](https://gitlab.com/groups/gitlab-org/-/epics/6313) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈 [Mentorship](https://gitlab.com/emchang/open-notebook/-/issues/10)🌈🌻

- [x] Finalise the conceptual object model for Jobs
- [x] Go thru the major PA epics
- [x] [Set up Alfred](https://www.youtube.com/watch?v=-tt-U5DHTUc) for those major PA epics
- [x] Reflect on my experience with the [mini pipeline graph contribution](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835#note_679555109) and ping Jeremy for feedback
- [x] Read the [article about YAML at Uber](https://eng.uber.com/streamlining-mobile-data-workflow-process/)
- [x] Reflect and plan next week + next milestone

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Lots of progress on the Pajamas issues! Learnt a ton from Jeremy's and Taurie's reviews. It was rewarding to slow down and write a [retrospective on my contribution](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835#note_679555109). Looking forward to their feedback!

#### What I learnt 

- Learnt how to do git rebase and shared my process with others

#### What didn't go so well? Why?

- The 360 feedback has been a bit stressful, but I'm happy to grow from every experience, even if it's uncomfortable. I got this.

</p>
</details>

## Next Week

<details><summary>Expand</summary>

- [ ] Finalise the proposal for [retrying all DSPs from the trigger job.](https://gitlab.com/gitlab-org/gitlab/-/issues/32559/) Should we start with an action in the job page if it's easier? And then if needed the bubble it up to the pipeline graph.
- [ ] Share my learnings around the work on manual jobs, variables, child pipelines. Could be an overview of what Pipeline Authoring UX has been up to this milestone.
- [ ] Move forward the discussions around the [mini pipeline graph issue](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/946)


<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Reflect back on the [async collab process with the Growth team](https://gitlab.com/gitlab-org/gitlab/-/issues/332248#note_620427152)
- [ ] Reflect back on the mentorship program
- [ ] [Review PA direction, do additional research and make notes](https://gitlab.com/dfosco/dfosco/-/blob/main/deployments-direction.md)
- [ ] [Try RICE framework](https://www.intercom.com/blog/rice-simple-prioritization-for-product-managers/)
- [ ] Create a new page for accessibility resources in Pajamas accessibility section
- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Read https://medium.com/better-programming/circleci-vs-gitlab-choose-the-right-ci-cd-tool-f920ce90ea09 
- [ ] Watch [Figma workshop](https://www.youtube.com/watch?v=sCRYJdKoaxE&mkt_tok=eyJpIjoiTVRsaE9XUXdOamRrTnpObSIsInQiOiJqU3FBdEhiQnRHSFZIaUdudVd6WjJTSVZcL0ZSY2hXa1R5TzBcL1lUNER4RER4bDJUQmpYZDMwamNHZHhQd1hTWk9nUWEzQ1VvVTh1UGxLaUtmWFluXC95QmlHOTR3WEwzVjNCVTU0ektjbEpCUzFqYWwxTEV0blZBc0t6d3E3ejVNaiJ9) 
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 
- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/

</p>
</details>
