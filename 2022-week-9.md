# Week 9 | February 28 - March 4 👁💗🤝✨

#### Main focus 

- [14.9](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/43) design issues

#### Milestone 

- [14.9](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/43)

## Tasks

#### Research

#### Design

- [x] CI Catalog design exploration for all pages
- [x] Refine the issue using Andy's file as an example
- [x] Gather feedback from Dov first
- [-] Share with the engineers, CI templates, Verify, UX for feedback
- [-] Read thru and refine the problem statement for the variables issue
- [-] Issue for identifying trigger jobs in the UI

#### Pajamas

- [x] Finish the to-do's for the [Drawer issue](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/376#note_835266675) 

#### Other

#### Focus Friday 🔮

🌻🌈 [CI/CD UX OKRs Q1](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/50) 🌻🌈 [FY23 Q1 & Q2](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [x] Draft of the PA UX direction (referencing the [UX dept FY23 direction](https://about.gitlab.com/handbook/engineering/ux/#fy23-direction)), with a section on piloting the UX DoD process (use[ Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and Veethika's UX pages as inspo)
- [x] Move PA Figma files to the Verify/PA project
- [-] Update my career dev plan [as per Rayana's recommendations](https://docs.google.com/document/d/1AfoPW1xAPEgdSmz0CWZWhydBXhhcO_QHO0ces8kKYR4/edit)
- [x] Provide feedback on the pipeline conceptual model
- [x] Reflect and plan next week  
- [x] Reflect on the [Pajamas maintainer trainee progress](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835) 

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Finished the first pass on the CI/CD catalog design exploration. It was great to work on something so different for once.
- Finally reorganized all Figma files!

#### What I learnt 

- Learnt about how component overrides work with layers and reviewed a related Figma contribution. 

#### What didn't go so well? Why?

- Had limited time because of OOO so didn't get to everything on my list, but that's OK.
- Feeling really bad about the trigger jobs epic. UX should've really handled it better. I have lots of learnings from this experience though so I know we'll improve with all future efforts.

</p>
</details>

## Next Week

<details><summary>Expand</summary>

- [ ] Make an MR to the PA handbook about the current UX strategy focus 
- [ ] Make MRs for [2 UI text issues](https://gitlab.com/groups/gitlab-org/-/issues?sort=created_date&state=opened&label_name[]=UI+text) (preferably PA and CI/CD, but also other high impact)
- [ ] Clean up the rest of the Figma files

<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 

</p>
</details>
