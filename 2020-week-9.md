# Week 9 | February 24 - February 28

Main focus: Make progress on 12.9 issues, 12.10 planning, Accessibility Course

## Milestone

12.9

## Tasks

- [x] Work on 2 separate discussion guides for [Q1 OKR: Validate Logging Maturity Level with Users](https://gitlab.com/gitlab-org/ux-research/issues/652) to include an OKR and additional questions discussion guides. Work on questions wording.
- [ ] Get started the making of [the demo environment for the OKR](https://gitlab.com/gitlab-org/gitlab/issues/207975)
- [x] [Prometheus Research](https://gitlab.com/gitlab-org/ux-research/issues/553) Synthesis and Insights creation
- [x] Iterate on [Design issue: Display annotation on the metrics chart](https://gitlab.com/gitlab-org/gitlab/issues/205029)
- [x] First pass designs for [Design issue: Add single timestamp to the log explorer time picker](https://gitlab.com/gitlab-org/gitlab/issues/201747)
- [ ] Create an IA structure for the [monitoring docs, think how to set up a tree test.](https://gitlab.com/gitlab-org/gitlab/issues/199420)
- [x] Check the [empty state CTA links for logs amd metrics](https://gitlab.slack.com/archives/C03MSG8B7/p1581465935405000)

## Admin

- [x] 12.10 planning
- [x] Send GitLab invoice and expense reports 

## Learning and Development

- [x] Complete 2 modules of Accessibility Course
- [ ] Write the blog post

### Unplanned tasks

