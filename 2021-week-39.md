# Week 38 | September 27 - October 1 👁💗🤝✨

<details><summary>Expand the week</summary>

#### Main focus 

- Solution validation planning

#### Milestone 

- [14.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/26#scope-of-work-for-ux)

## Tasks

#### Research

- [x] Create a plan for [Solution Validation: Pipeline Editor usability testing](https://gitlab.com/gitlab-org/ux-research/-/issues/1590)
- [x] Create a discussion guide based on [this draft](https://gitlab.com/gitlab-org/ux-research/-/issues/1590#note_689320593)
- [x] Ping UX resarch and Rayana to suggest improvements to my discussion guide
- [x] Prepare the [test project](https://gitlab.com/nadia_sotnikova/test-ruby-app/-/pipelines)

#### Design

- [x] Design exploration for [Design: Surface the links to the CI/CD configuration includes](https://gitlab.com/gitlab-org/gitlab/-/issues/339469)
- [x] Work on the job object map and layout in Figma

#### Other

- [x] Add to FE call agenda: discuss the [mini pipeline graph component updates](https://gitlab.com/gitlab-org/gitlab/-/issues/340827), [manual job permissions issue](https://gitlab.com/gitlab-org/gitlab/-/issues/339922#note_673800253), [running manual jobs with variables issue](https://gitlab.com/gitlab-org/gitlab/-/issues/32712/)

</details>

#### Focus Friday 🔮 

🌈🌻 [Personal Development Goals Q3](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46) 🌻🌈 [Q4](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/50) 🌻🌈 [UX department OKRs Q3](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1660) 🌈🌻 [Verify Hive OKRs Q3](https://gitlab.com/groups/gitlab-org/-/epics/6313) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈 [Mentorship](https://gitlab.com/emchang/open-notebook/-/issues/10)🌈🌻

- [x] Share the [Hacktoberfest issue](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/general/-/issues/102) with the CI/CD UX team to remind to discuss with their PMs
- [x] Start a UX Slack thread (maybe in UX co-working?) to ask how they communicate with their engineers
- [x] Provide feedback in UX co-working
- [x] Start recruiting with [the screener](https://gitlab.fra1.qualtrics.com/jfe/form/SV_9LWCNAFQZl5apJI), send it to those who DMed me, reply to one of Michaels's tweets and tag Dov
---
- 🌟 _lvl collaborative flower_ 🌺
---
- [x] Pings catch-up
---
- 🌟🌟 _lvl issue slayer_ 🔥
---
- [-] Reflect on what UX leadership shared in UX weekly and see if I can integrate more of it into my work
- [-] Trim down the Q4 plan (scope down the tasks, and use themes as long term goals for the next year)
- [x] Reflect and plan next week
---
- 🌟🌟🌟🌟 _lvl strategy queen_ 💃
---

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>

- [ ] Design exploration for [Design: Surface the links to the CI/CD configuration includes](https://gitlab.com/gitlab-org/gitlab/-/issues/339469)
- [ ] Work on MVC guidelines for the [object model contributions to Pajamas](https://gitlab.com/groups/gitlab-org/-/epics/6735#note_680522526)
- [ ] Propose a new MR for the PA JTBDs to have 2-3 main JTBDs + underlying user stories. See this old MR for reference.
- [ ] Run a pilot of the test myself + ask a PA engineer to pilot it
- [ ] Schedule the interviews for the pipeline editor usability testing (use Calendly) + share them with the team to join
- [ ] Add the participants to the test project as maintainers

<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Reflect back on the [async collab process with the Growth team](https://gitlab.com/gitlab-org/gitlab/-/issues/332248#note_620427152)
- [ ] Reflect back on the mentorship program
- [ ] [Review PA direction, do additional research and make notes](https://gitlab.com/dfosco/dfosco/-/blob/main/deployments-direction.md)
- [ ] [Try RICE framework](https://www.intercom.com/blog/rice-simple-prioritization-for-product-managers/)
- [ ] Create a new page for accessibility resources in Pajamas accessibility section
- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Read https://medium.com/better-programming/circleci-vs-gitlab-choose-the-right-ci-cd-tool-f920ce90ea09 
- [ ] Watch [Figma workshop](https://www.youtube.com/watch?v=sCRYJdKoaxE&mkt_tok=eyJpIjoiTVRsaE9XUXdOamRrTnpObSIsInQiOiJqU3FBdEhiQnRHSFZIaUdudVd6WjJTSVZcL0ZSY2hXa1R5TzBcL1lUNER4RER4bDJUQmpYZDMwamNHZHhQd1hTWk9nUWEzQ1VvVTh1UGxLaUtmWFluXC95QmlHOTR3WEwzVjNCVTU0ektjbEpCUzFqYWwxTEV0blZBc0t6d3E3ejVNaiJ9) 
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 
- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/

</p>
</details>
