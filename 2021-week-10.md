# Week 10 | March 8 - 12

#### Main focus 

- Finalise the new pipeline graph designs and kick off planning breakdown

#### Milestone 

[13.10](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/5#scope-of-work-for-ux)

## Tasks

<details><summary>Expand</summary>
<p>

#### Design

- [x] Finalise the new pipeline graph designs and kick off planning breakdown
- [x] Explore high level ideas for the CI job templates. Research, open questions, suggestions.

#### Other

- [x] Write out a draft of the CMS scenario, discuss with Dov and Lorie
- [x] Work on [descriptions for CI/CD templates](https://gitlab.com/gitlab-org/gitlab/-/issues/290277#note_514618449)
- [x] Link issues/ create new issues based on https://docs.google.com/document/d/1abuA7PhY4Ep-OHV7V5UUDNllW-RREDXTDFwVxNCyYwc/edit 

#### Focus Fridays

🌈🌻 [Personal Development Goals Q1](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/29) 🌻🌈 [UX department OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1501) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈 [CI/CD Growth and Development Q1](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/33) 🌈🌻

- [x] The Process video series: How I plan for and track my weekly, quarterly and bi-yearly goals.
- [x] [Finish the Accessibility Course](https://www.linkedin.com/learning/accessibility-for-web-design/welcome?u=2255073) and share [my notes](https://docs.google.com/document/d/13MS7D1bbEuyFwTayaZtgkQ1T0h6az77p_IvTPu0VayI/edit) in [CI/CD tracking](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/33) and the [OKR issue comments](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10468)
- [-] ~~Watch 3 most interesting talk recordings from the accessibility conference~~ The recordings were not available yet, so I'm moving this to next week.
- [x] Provide feedback in UX co-working
- [x] Weekly retro
- [x] Plan next week

</p>
</details>

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- I was able to gather lots of helpful feedback from both UX team and engineers during an internal async solution validation for the new pipeline graph.
- The CMS scenario is coming along nicely. Hopefully we can pilot it next week.
- Found the time to finish the Accessibility Course, and am planning to apply what I learnt in a [pipeline graph accessibility audit.](https://gitlab.com/gitlab-org/gitlab/-/issues/324294) 
- Unblocked us to start working on CI/CD templates description tooltips by writing up descriptions. TW can take it over to polish things now, and soon CI/CD templates will be much easier to browse!

#### What I learnt 

- I'm not my most productive self when stuck in a quarantine hotel room for 2 weeks. Just need to remember that it's OK and I will be more productive next week once I can go to a coworking space. 💗

#### What didn't go so well? Why?

- I'm really struggling with moving forward the new pipeline graph designs into implementation. There seem to be lots of misunderstanding and communication problems on the team, and it seems like we didn't schedule enough time for solution validation and planning breakdown discussions. There's some disconnect between UX + Product and Engineering. This should be #1 priority to figure out next week. For now I have a meeting with our EM to discuss the problems. Hopefully this conversation will give me some insights into why we're having so much friction and I can come up with actionable steps to help us find a better process for collaboration.

- The problems with communication has been really slowing me down and adding lots of overhead to my work. 

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- Watch 3 talk recordings from the Axe conference:
    - [Making Motion Inclusive](https://axe-con.com/event/making-motion-inclusive/)
    - [Accessible Data Visualizations 101](https://axe-con.com/event/accessible-data-visualizations-101/) (stoked about this one because 🔥🔥PIPELINE GRAPH🔥🔥)
    - [The UX Behind axe DevTools](https://axe-con.com/event/the-ux-behind-axe-devtools/)
- PA CMS pilot run (run the scenario myself and with someone from the team)
- Usability test scenario
- CMS and usability test recruiting + scheduling
- [ ] Explore [better wording for Needs view](https://gitlab.com/gitlab-org/gitlab/-/issues/323150#note_524326610) and update [issues.](https://gitlab.com/groups/gitlab-org/-/epics/4509#note_525783961)
- Move forward the discussions around job templates
- Move forward the discussions around pipeline graph
- Move forward Pajamas issues + MRs
</p>
</details>

## Plan for later

<details><summary>Expand</summary>
<p>
 
- [ ] Walkthrough about the Visual Pipeline Builder MVC, the sketching exercise, Think Big + Think Small, etc.
- [ ] Share my experience with Focus Fridays widely at the end of Q1
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Read https://medium.com/better-programming/circleci-vs-gitlab-choose-the-right-ci-cd-tool-f920ce90ea09 
- [ ] Watch [Figma workshop](https://www.youtube.com/watch?v=sCRYJdKoaxE&mkt_tok=eyJpIjoiTVRsaE9XUXdOamRrTnpObSIsInQiOiJqU3FBdEhiQnRHSFZIaUdudVd6WjJTSVZcL0ZSY2hXa1R5TzBcL1lUNER4RER4bDJUQmpYZDMwamNHZHhQd1hTWk9nUWEzQ1VvVTh1UGxLaUtmWFluXC95QmlHOTR3WEwzVjNCVTU0ektjbEpCUzFqYWwxTEV0blZBc0t6d3E3ejVNaiJ9) 
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 
- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/

</p>
</details>
