# Week 13 | March 28 - April 1 👁💗🤝✨

#### Main focus 

- [14.10](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/46) design issues
- Plan PTO
- Plan 15.0

#### Milestone 

- [14.10](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/46)

## Tasks

#### Research

#### Design

- [x] Finalise the design discussions for the application limits issue
- [x] Refine user flows for the CI catalog
- [x] Create a CI catalog home page mock-up and a template page mock-up
- [x] Update the CI Catalog issue and ping the team for discussion
- [x] Update the issue for [raw variables](https://gitlab.com/gitlab-org/gitlab/-/issues/217309#note_886844074)
- [x] Review https://gitlab.com/gitlab-org/gitlab/-/issues/337282
- [x] Design for [authrorised variables](https://gitlab.com/gitlab-org/gitlab/-/issues/297250#note_891093731)

#### Pajamas

- [x] [Pajamas: Clarify drawer usage guidelines](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1237)

#### Other

- [x] Record a walkthrough for the 14.10 issues for Veethika and Katie ([UX coverage list](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1913#note_890845994))
- [x] Plan PTO (reach out to the engineers about the upcoming MR reviews, get things ready for my backups)
- [x] Find frontend issues to recommend for implementation for 15.0, assign "UX" and "Seeking community contributions"
- [x] Plan 15.0

#### Focus Monday 🔮 

🌻🌈 [CI/CD UX OKRs Q1](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/50) 🌻🌈 [FY23 Q1 & Q2](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [x] Plan problem and solution validation for the FY23 (see the notes in Dov's agenda) https://gitlab.com/gitlab-org/ux-research/-/issues/1750#note_872377941
- [ ] Reflect and plan the week after vacation 
- [x] Pings catch-up

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>

- [ ] Refine the [Pipeline Authoring UX page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100082) and request a review from Rayana ([Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and [PE](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/) pages for structure)

<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
