# Week 4 | January 25 - 29

#### Main focus 

- 13.9 design issues

#### Milestone 

[13.9](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/4#note_481082352)

## Tasks

#### Design

- [x] Iterate on [Managing multiple files in Pipeline Editor](https://gitlab.com/gitlab-org/gitlab/-/issues/276904). Explore how to create new files. Get feedback from PA team and UX team. 
- [x] Write content for the MVC Pipeline Editor drawer and get feedback from Dev. Evangelism team, tech writing team, PM, engineers
- [x] Reach out to owners of [templates](https://gitlab.com/gitlab-org/gitlab/-/issues/290079), share the issue again in Product and UX
- [x] Follow-up tasks for the [inline component](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/778#note_484859157)
- [x] Follow-up tasks for the table chart legend
- [ ] Create an illustration for [Pipeline Editor popover](https://gitlab.com/gitlab-org/gitlab/-/issues/276480)

#### Other

- [x] Reflect on [my OKR](https://gitlab.com/groups/gitlab-com/-/epics/1240) setting experience, record a 5 minute video about it, the highs and lows, all the mistakes. Upload to Youtube Unfiltered. Share on UX and PA team Slack, UX weekly and my Twitter.
- [x] Document user stories and edge cases for Pipeline Editor. Create an issue.
- [ ] Plan quick solution validation for Multi-file in Pipeline Editor for next week
- [ ] Go thru the growth Mural, OKRs and my performance review. Plan what I want to focus on for the next quarter and 6 months.
- [x] Create an MR to add the JTBD for file management "I want to easily access all CI/CD files from one place so I can efficiently edit my CI/CD configuration"

#### Learning and Development

## Wins and Lessons

#### Wins of the week 

- Took time to thoroughly test Pipeline Editor and suggest MVC improvements. Added issues to https://gitlab.com/groups/gitlab-org/-/epics/5202
- Also tested the beta of the new Pipeline graph and [provided feedback.](https://gitlab.com/groups/gitlab-org/-/epics/5260) It felt great to catch-up with the details of the current state of things. I need to do it as often as I can because it triggers new creative ideas for how to solve interconnected problems. 
- Shipped a big design update for the multi-file management in Pipeline Editor. Really happy with how much time I was able to dedicate to considering different use cases. It definitely helps to have more time now that I'm farther away from the development. 
- Kicked off an effort to document PA user stories and missing JTBDs. We can start by identifying the different use cases we're solving for, and then fill in the gaps. https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/11
- Recorded a video about my experience with setting personal OKRs 

#### What I learnt 

- Interesting question I read in The Hustle newsletter: "What’s your secret sauce? And how can you lean into that more this year?"

#### What didn't go so well? Why?

- Was doing lots of deep work on designs, moving forward issues, and got behind on emails (issue comments). Perhaps can try to schedule 1,5 hours per day automatically to catch-up with to-do's. Otherwise I end up doing all the big effort work and maybe someone's blocked by my response. 

-----------

## Next Week

- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/



## Plan for later

- [ ] Rescore the Pipeline Authoring experience after the drawer is released
- [ ] Fix the [JTBD page](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-authoring/jtdb/#jtbd) links
- [ ] Draft for Pipeline Authoring UX page. Here's the [UX CI page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/continuous-integration/) as an example. Once merged, need to update the UX CI/CD, UX Verify, and Verify Pipeline Authoring pages.
- [ ] Share about my process for getting to a visual builder MVC with a team exercise, Think Big, Think Small, and changing design direction. 
- [ ] Share my [wins and lessons weekly retro process](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/2020-week-48.md#wins-and-lessons) in a Loom video. Upload to Unfiltered. Share with the UX team along with the link. 
- [ ] Organise PA research initiatives [like Veethika did for CI](https://gitlab.com/groups/gitlab-org/-/epics/4927#note_448315299) 👌👌
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 

## Current OKRs to focus on

#### UX

- [UX Q4 OKRs (due Jan 30, 2021)](https://gitlab.com/gitlab-org/verify-and-release-ux-team/-/issues/22#note_436601817)
