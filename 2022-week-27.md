# Week 27 | July 6 - July 8  👁💗🤝✨

#### Main focus 

#### Milestone 

- [15.2](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/61)

## Tasks

#### Research

- [x] Finish summarising the [solition validation discussions](https://gitlab.com/gitlab-org/ux-research/-/issues/1942#research-insights-summary) in the [design issue](https://gitlab.com/gitlab-org/gitlab/-/issues/359047#note_1012837731)

#### Design

- [x] Summarise the improvements to address in the MVC iteration issue (to be designed in the next weeks)
- [x] Organise the epic with issues and refine epic description

#### Pajamas

- [ ] Finish the Tanukis update

#### Other

- [x] Finalise the [PA roadmap](https://app.mural.co/t/gitlab2474/m/gitlab2474/1654692090314/fb8ffd76fbd73b4a15f00eeef9dff0d8c5b8175b?sender=dhershkovitch7730)

#### Focus Friday 🔮 

🌻🌈 [FY23 Q2 CI/CD UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978#fy23-q2-okrs-for-cicd-ux) 🌻🌈 [FY23 Q2-Q3 Career Development](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [ ] Refine the [Pipeline Authoring UX page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100082) with info on merge request reviews for PA and request a review from Rayana ([Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and [PE](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/) pages for structure)
- [x] Provide feedback in UX co-working
- [x] Reflect on and plan next week

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Had a chance to present the research insights to the PA team in sync, it was nice to have that face-to-face time
- Had lots of MRs to review this week, it went relatively smoothly. The Gitpod runner set-up didn't work, still troubleshooting.
- Slowly starting to feel better mentally and becoming more productive.
- Found the time to collaborate in ux-coworking this week. I really enjoy collaborating with other designers on small, actionable improvements. I want to make it a priority every week to have the space for acting on such spontaneous oppoerunities. 

#### What I learnt 

- Learnt that one needs 2 different runners for different CI features.

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Iterate on the pipeline components MVC proposal based on solution validation
- [ ] Create any necessary follow-up issues for the pipeline components insights / future improvements
- [ ] Refine the design issue and close the sol val issue
- [ ] Create a video summary of the pipeline components research insights, MVC direction, next steps and future improvements.
- [ ] Share in the UX, CI-CD UX, PA, Verify channels
- [ ] Refine the [Q2-3 Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) based on the feedback from Rayana and scope down ruthlessly

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
