# Week 45 | November 8 - November 12 👁💗🤝✨

#### Main focus 

- Plan UX scorecard with Jarek
- Finalise 14.5 design issues

#### Milestone 

- [14.5](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/27#scope-of-work-for-ux)

## Tasks

#### Research

- [x] Send out invites to more participants for the pipeline editor usability testing (send out to [the other participants](https://docs.google.com/spreadsheets/d/1QQ4u-_ausfqxAim5mPqIbC45cAZxf-wk3QAn8zejmm0/edit#gid=0) if these don't schedule this week)
- [x] Translate and write up the [notes](https://docs.google.com/document/d/1RL8g2g85L1uCPSIhE1yDjRYvl17HmQFFcmQogDJVIQU/edit#) for the X5 call
- [x] Create the [UX Scorecard plan for Jarek](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12593)

#### Feature design

- [x] Design exploration for the [pipeline simulation](https://gitlab.com/gitlab-org/gitlab/-/issues/337282/)
  - [x] Get team's feedback on the new proposal for pipeline simulation (see prototype)

#### Strategic planning, OKRs, process

- [x] Look thru latest 360 feedback results, complete the [performance worksheet](https://docs.google.com/spreadsheets/d/1aXXpjusLXlkEPMEjVL2ef8lymZYi_FKiwaSKwzvd9iQ/edit#gid=0) and complete the [talent assessment tracker issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1775)
- [x] Share the upcoming time off + coverage issue with the PA, Ops and UX teams
- [x] Finalise [14.6](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/30#scope-of-work-for-ux)

#### Focus Friday 🔮 

🌈🌻 [Personal Development Goals Q3](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/46) 🌻🌈 [Q4](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/50) 🌻🌈 [UX department OKRs Q4](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756) 🌈🌻 [Verify Hive OKRs Q3](https://gitlab.com/groups/gitlab-org/-/epics/6313) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈 

Other Tasks 💫

- [x] Complete [Reliability Training](https://gitlab.com/gitlab-org/verify-stage/-/issues/137)
- [-] Move forward [the objects contribution guidelines](https://gitlab.com/groups/gitlab-org/-/epics/6735#note_698985377)
- [x] Reflect and plan next week

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Set up a process for UX scorecard for Q4 OKR
- Got great insights from the call with a customer
- Moved forward design iteration on the pipeline simulation

#### What I learnt 

#### What didn't go so well? Why?

- Feeling burnt out. Moving across the world and trying to keep up 100% work capacity has been challenging. Overall the past few weeks have been very draining. I think I started falling behind because I was trying to travel, work at 100%, do the IPO related stuff, complete all OKRs and personal goals, + work on my side projects. It was just all too much. I'm excited for vacation, and afterwards I need to get serious about setting boundaries and saying no to things. It's OK to not perform at 100%, 100% of the time. The performance review definitely put some pressure on me to keep going too. I'll need to find a better balance once I'm back to work.

</p>
</details>

## Next Week

<details><summary>Expand</summary>

- [ ] Take the [psychological safety training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12592), check off the [Verify issue too](https://gitlab.com/gitlab-org/verify-stage/-/issues/128)
- [ ] Ask Jeremy for feedback async based on Rayana's suggestions (see our agenda)
- [ ] Propose a new MR for the PA JTBDs to have 2-3 main JTBDs + underlying user stories. See this old MR for reference.
- [ ] Draft of PA UX Page that refers to the relevant parts in the PA handbook and UX handbook
- [ ] Read the [Harness blog](https://harness.io/blog/gitlab-and-harness/) + test the app again
- [ ] Update the empty state (illo) for the [retry trigger job issues](https://gitlab.com/groups/gitlab-org/-/epics/6947)
- [ ] Add guidelines for UX planning to the handbook based on [this issue](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/52#note_714720750)
- [ ] Finalise the designs for Retry manual jobs with variables
  - [ ] Finalise the proposal (permissions and updated hide/reveal pattern) for the [retry the jobs with variables issue](https://gitlab.com/gitlab-org/gitlab/-/issues/32712/)
  - [ ] Create more follow-up issues and organise the [variables epic](https://gitlab.com/groups/gitlab-org/-/epics/6262)
  - [ ] Record a video to share the [results of the solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1617) + next steps and epic updates
  - [ ] Share the update on UX, PA, Verify Slack linking to the insights, feature proposal, and future vision, and ask for everyone's input and feedback
- [ ] Organize the implementation of [changes for the mini pipeline graph](https://gitlab.com/gitlab-org/gitlab/-/issues/340827#note_695871872)

<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Create a library of shared tags to be used across Verify (see [this](https://about.gitlab.com/handbook/engineering/ux/dovetail/#creating-cross-stage-awareness-using-shared-tags) and [this](https://dovetailapp.com/blog/global-project-tags-taxonomy-research-repository/))
- [ ] [Review PA direction, do additional research and make notes](https://gitlab.com/dfosco/dfosco/-/blob/main/deployments-direction.md)
- [ ] [Try RICE framework](https://www.intercom.com/blog/rice-simple-prioritization-for-product-managers/)
- [ ] Create a new page for accessibility resources in Pajamas accessibility section
- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Read https://medium.com/better-programming/circleci-vs-gitlab-choose-the-right-ci-cd-tool-f920ce90ea09 
- [ ] Watch [Figma workshop](https://www.youtube.com/watch?v=sCRYJdKoaxE&mkt_tok=eyJpIjoiTVRsaE9XUXdOamRrTnpObSIsInQiOiJqU3FBdEhiQnRHSFZIaUdudVd6WjJTSVZcL0ZSY2hXa1R5TzBcL1lUNER4RER4bDJUQmpYZDMwamNHZHhQd1hTWk9nUWEzQ1VvVTh1UGxLaUtmWFluXC95QmlHOTR3WEwzVjNCVTU0ektjbEpCUzFqYWwxTEV0blZBc0t6d3E3ejVNaiJ9) 
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 
- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/

</p>
</details>
