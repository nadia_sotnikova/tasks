# Week 20 | May 17 - May 20 👁💗🤝✨ 
#### Main focus 

- Auto DevOps design sprint
- The CI/CD templates experience research and discovery 

#### Milestone 

[14.0](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/15#scope-of-work-for-testing)

## Tasks

#### Research and Design

- [x] Work on the [Senior Product Designer justification doc](https://docs.google.com/document/d/1d0Tk6usyOJNkNx7HmbtwmA0pNgw2k0Gr6yxRX6mE-fE/edit?usp=sharing) (examples [1](https://docs.google.com/document/d/1qZByNLd4hDgKXlN3jnsTLijbnfcg8pAL9ojkBqP8H4E/edit) and [2](https://docs.google.com/document/d/1wwg9URIwgjv0Ee18ssiQcfuWPeanX74aZmQeFd3NOtA/edit))
- [x] [Auto DevOps Design Sprint](https://gitlab.com/groups/gitlab-org/-/epics/5939)
- [x] Consume research and prepare questions and discussion around [CI/CD template manageemnt experience](https://gitlab.com/gitlab-org/gitlab/-/issues/329848) and enforced pipelines with Compliance on Wednesday
- [x] Write up a list of questions to ask our customers based on the research and discussions with compliance management, coordinate with Dov to schedule the interviews
- [x] Review https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/845#note_578792730 

#### Other

- [x] Read first two chapters of the Microcopy book
- [x] Reflect on the week + next week planning
- [x] [Comment on 13.12 PA retro](https://gitlab.com/gl-retrospectives/verify-stage/pipeline-authoring/-/issues/8#note_578991348)
- [x] [Comment on UX 13.12 Retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/35) and share in the CI/CD UX channel

#### Focus Friday (🌴)

<details><summary>Expand</summary>
<p>

🌈🌻 [Personal Development Goals Q2](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/40) 🌻🌈 [UX department OKRs Q2](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1603) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈

</p>
</details>

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Started reviewing Taurie's PJs contribution, I really want to go back to Pajamas stuff! https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/845#note_578792730 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Finish the [Senior Product Designer justification doc](https://docs.google.com/document/d/1d0Tk6usyOJNkNx7HmbtwmA0pNgw2k0Gr6yxRX6mE-fE/edit?usp=sharing) (examples [1](https://docs.google.com/document/d/1qZByNLd4hDgKXlN3jnsTLijbnfcg8pAL9ojkBqP8H4E/edit) and [2](https://docs.google.com/document/d/1wwg9URIwgjv0Ee18ssiQcfuWPeanX74aZmQeFd3NOtA/edit))
- [ ] Tag the 2 customer videos with shared tags
- [ ] Customer interviews and iterate on user flows for CI/CD template management
- [ ] Share most valuable resulting artifacts from the ADO with CI/CD UX team and PA team about the vision I created, and the winning vision, as well as my experience from participating in async design sprint
- [ ] Iterate on the [visual builder issue](https://gitlab.com/gitlab-org/gitlab/-/issues/327210) based on feedback in Slack and issue, and create a prototype to test
- [ ] Listen to 2 top The Bad Conference recordings and [take notes](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/39)

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] [Review PA direction, do additional research and make notes](https://gitlab.com/dfosco/dfosco/-/blob/main/deployments-direction.md)
- [ ] [Try RICE framework](https://www.intercom.com/blog/rice-simple-prioritization-for-product-managers/)
- [ ] Competitor analysis for Appsmith.com
- [ ] Create a new page for accessibility resources in Pajamas accessibility section
- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Run a competitor analysis for PA ([example](https://gitlab.com/gitlab-org/gitlab/-/issues/326285))
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Read https://medium.com/better-programming/circleci-vs-gitlab-choose-the-right-ci-cd-tool-f920ce90ea09 
- [ ] Watch [Figma workshop](https://www.youtube.com/watch?v=sCRYJdKoaxE&mkt_tok=eyJpIjoiTVRsaE9XUXdOamRrTnpObSIsInQiOiJqU3FBdEhiQnRHSFZIaUdudVd6WjJTSVZcL0ZSY2hXa1R5TzBcL1lUNER4RER4bDJUQmpYZDMwamNHZHhQd1hTWk9nUWEzQ1VvVTh1UGxLaUtmWFluXC95QmlHOTR3WEwzVjNCVTU0ektjbEpCUzFqYWwxTEV0blZBc0t6d3E3ejVNaiJ9) 
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 
- [ ] Read https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/

</p>
</details>
