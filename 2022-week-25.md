# Week 25 | June 20 - June 23  👁💗🤝✨

#### Main focus 

#### Milestone 

- [15.2](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/61)

## Tasks

#### Research

- [x] Finish the interviews for pipeline components MVC
- [ ] Tag the interviews in Dovetail

#### Design

- [x] Update [this issue proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/217309#note_991832036)

#### Pajamas

- [ ] Finish the Tanukis update
- [ ] Pajamas traineeship checkin ([see here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835#note_929756938))
- [x] [Pajamas: Small "helping users" page update](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1366#note_969425728)

#### Other

- [x] 15.1 UX and PA retro
- [x] [Self-assessment for the mid-year check-in](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2002)
- [ ] Put research and other important issues into the [PA roadmap](https://app.mural.co/t/gitlab2474/m/gitlab2474/1654692090314/fb8ffd76fbd73b4a15f00eeef9dff0d8c5b8175b?sender=dhershkovitch7730)
- [ ] Refine the [Q2-3 Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) based on the feedback from Rayana and scope down ruthlessly
- [ ] Refine the [Pipeline Authoring UX page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100082) with info on merge request reviews for PA and request a review from Rayana ([Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and [PE](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/) pages for structure)
- [ ] Provide feedback in UX co-working
- [ ] Reflect on and plan next week

#### Focus Friday 🔮 

🌻🌈 [FY23 Q2 CI/CD UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978#fy23-q2-okrs-for-cicd-ux) 🌻🌈 [FY23 Q2-Q3 Career Development](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- [Made an MR](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2911) to refactor Helping users docs
- [Provided feedback to Gina on a Pajamas MR](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2901#note_996941093)
- Reviewed a gitlab-ui drawer MR and created a [follow-up issue](https://gitlab.com/gitlab-org/gitlab-ui/-/issues/1859) to define the footer usage guidelines

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
