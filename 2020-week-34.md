# Week 34 | August 17 - August 21 ️✨🚀

#### Main focus 

Finalise 13.3 issues
Vue component migration
Figure out what's happening

#### Milestone

13.4

## Tasks

#### Design

- [x] Design for ["Duplicate dashboard" experience](https://gitlab.com/gitlab-org/gitlab/-/issues/220175)
- [x] Finalise design for "Add panel MVC2" and "Edit and Delete panels". Create follow-up issues. Groom issues.
- [x] Document [solution validation results](https://gitlab.com/gitlab-org/ux-research/-/issues/961#rocket-outcomes) in Dovetail + create actionable insight issues ([see comment](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1308#note_395453454)) https://gitlab.com/gitlab-org/gitlab/-/issues/235982 https://gitlab.com/gitlab-org/gitlab/-/issues/235141
- [x] 2 issues from the OKR
- [x] Provide feedback to Veethika
- [x] Go over CI/CD resources
 
#### Other

- [ ] Figure out [Figma team stuff](https://www.figma.com/proto/73OcYdBfOaK2xlChC3tbNX/Figma-for-GitLab?node-id=142%3A8&viewport=79%2C270%2C0.05665680393576622&scaling=contain), move files
- [ ] Record a Figma walkthrough for engineers, share with the UX team https://gitlab.slack.com/archives/CK06WE78S/p1597666135241100

## Wins and Lessons

#### Wins of the week

- Yay, got offered an oppotunity to become a Pajamas maintainer trainee! Can't wait. https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/638

#### Lessons of the week

-----------

## Next Week

- [ ] 2 issues from the OKR
- [ ] Onboarding with Verify
- [ ] [Pajamas design system training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835)

## Learning and Development

- [Pajamas Design System maintainer training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835)

## Current OKRs to focus on

#### UX

- [UX Q3 OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8363)
- [UX OPS issue to track all OKR issues](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1308)

## Plan for later

- [ ] The career development Mural exercise. 
- [ ] Make a case for UI Polish and UX debt on our team, collect UX research evidence that affects NPS score
- [ ] Summarize the docs card sort process following [Justin's template](https://docs.google.com/document/d/1_Nx1wBZ7zTFRCVNKMMJfNywjdAYQIdkY5ALjMK7kbzw/edit#heading=h.gq9xxzqffb0h) (see [issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1213))
- [ ] Find some [weird error messages](https://gitlab.com/groups/gitlab-org/-/epics/3327) to fix
