# Week 2 | January 10 - January 14 👁💗🤝✨

#### Main focus 

- Catching up

#### Milestone 

- [14.7](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/36)

## Tasks

#### Research

- [x] Sumarise the results of the [solution validation for pipeline simulation](https://gitlab.com/gitlab-org/ux-research/-/issues/1692)
- [x] Update the issue and ping the engineers and Dov
- [x] Create issues for the further iterations

#### Design

- [x] Iterate on the designs for the pipeline simulation MVC

#### Other

- [x] Iterate on the Job conceptual model (see comments in Figma) + create an issue to get feedback from the engineers
- [x] Share the Object overview page and share about the next steps we're working on
- [x] Share the call recording on ML pipelines
- [x] Go through [RICE prioritization](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/45#note_797364810)
- [x] Plan 14.8

#### Focus Friday 🔮 

🌻🌈 [Q4](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/50) 🌻🌈 [UX department OKRs Q4](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1756) 🌈🌻 [6-month Plan](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/PersonalGoals-FY22-Q1-Q2.md) 🌻🌈 

- [x] Provide feedback on the work from the [CI/CD weekly](https://docs.google.com/document/d/1fCMY77lzlsjHaCeqEY6pIw_K4bS_JYlfYlLJpPPJfKQ/edit#)
- [x] Analyze the PA JTBDs and [draft new JTBDs in FigJam](https://www.figma.com/file/jVfLsPVuDOfLCq6PDspvJQ/Pipeline-Authoring-JTBDs?node-id=0%3A1)
- [x] Reflect and plan next week  
- [x] Pings catch-up

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- Caught up from OOO pretty quickly, thank goodness everyone was on vacation. :D
- Analysed the pipeline simulation solution validatioin results and iterated on the designs
- Met with Eduardo about ML pipelines, learnt about a new pipelines use case for Data Scientists
- Started the PA JTBDs restucturing

#### What I learnt 

- Data scientists use pipelines for MLOps and have similar needs to anyone iterating on a pipeline config - they want easy onboarding, ability to run a pipeline without having to make a change, and ability to run parts of a pipeline to iterate in chunks.
- It's very important to take the time every day to rest and unwind. At times it felt like I was moving a bit slowly, but I ended up being very productive as a whole. Slow and steady is better than burnout fast.

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>

- [ ] Create an MR for the Object issue template
- [ ] Update the pipeline simulation MVC proposal, referring to the solution validation results and ping the team to start talking about the implementation
- [ ] Share the pipeline simulation solution validation results + next steps with UX, Verify and PA teams
- [ ] Complete the [Drawer audit](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/376#note_750204850) and summarise the findings/ problems in the issue.
  - [ ] Create a proposal for the design and documentation updates if needed
- [ ] Make an MR to the PA handbook about the current UX strategy focus 
- [ ] Draft of PA UX Page that refers to the relevant parts in the PA handbook and UX handbook 
- [ ] Propose a new MR for the PA JTBDs to have 2-3 main JTBDs + underlying user stories. See this old MR for reference.
- [ ] Reflect on the [Pajamas maintainer trainee progress](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835) (bi-weekly)  
- [ ] Finalise the permissions proposal for retrying manual job with updated variables
  - [ ] Create more follow-up issues and organise the [variables epic](https://gitlab.com/groups/gitlab-org/-/epics/6262)
  - [ ] Share the update on UX, PA, Verify Slack linking to the insights, feature proposal, and future vision, and ask for everyone's input and feedback

<p>

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Share the experience with improving UX collaboration on PA team widely (blog post and video?)
- [ ] Check out Andy's strategic approach in [this epic](https://gitlab.com/gitlab-org/gitlab/-/issues/294062)
- [ ] Make an MR to outline process for [evangelising GitLab UX online](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1419#note_457542331). Share with the UX team in Slack and UX Weekly.
- [ ] Update my ReadMe & share 

</p>
</details>
