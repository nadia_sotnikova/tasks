# Week 37 | Last week!  👁💗🤝✨

#### Main focus 

- Offboarding
- Transition

#### Milestone 

- [15.4](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/68#scope-of-work-for-ux)

## Tasks

#### Research

#### Design

- [x] Design for [Design: Show information about Runner set-up during CI/CD onboarding](https://gitlab.com/gitlab-org/gitlab/-/issues/290011)
- [x] Design for [Error if defined secrets do not exist in vault](https://gitlab.com/gitlab-org/gitlab/-/issues/353080)
- [x] Design for the job page variables tab

#### Beautifying UI / Pajamas

- [ ] Beautifying UI MR
- [x] Organise the designs for Beautifying UI

#### Other

- [x] https://about.gitlab.com/handbook/people-group/offboarding
- [x] [Finish the JTBDs](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/110019)
- [x] Finalise a [UX transition issue.](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2101) (also see [this doc](https://docs.google.com/document/d/1jUHgm14oxtsS-kZ2cBLcQILf6BDl5etSPLtE6AEF9Gk/edit#))
- [x] Check on the OKRs, update the OKR issues with the latest updates

#### Focus Friday 🔮 

🌻🌈 [FY23 Q2 CI/CD UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978#fy23-q2-okrs-for-cicd-ux) 🌻🌈 [FY23 Q2-Q3 Career Development](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [ ] Install OS to the old laptop
- [ ] Migrate to the old laptop

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Wipe the new laoptop
- [ ] Buy back the new laptop

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

</p>
</details3
