# Week 44 | October 28 - November 1

Conducting interviews for [Error Tracking Research](https://gitlab.com/gitlab-org/ux-research/issues/480)

## Milestone

12.5 Week 2

### Tasks   

- [x] [Interviews for Error Tracking Research](https://gitlab.com/gitlab-org/ux-research/issues/480)
    - [x] Call with Greg
    - [x] Call with Petr
    - [x] Call with Chuck

- [x] Complete [Designs for errors view on mobile](https://gitlab.com/gitlab-org/gitlab/issues/34433)
    - [x] Work on the Detail View of Sentry Error on Mobile
    - [x] Upload specs to GL https://gitlab.com/gitlab-org/gitlab-design/blob/master/CONTRIBUTING.md#superpowers-

- [x] Move forward [Filter dropdown hover state](https://gitlab.com/gitlab-org/gitlab/issues/28965#note_234635614)
- [ ] Move forward [Button states in IDE](https://gitlab.com/gitlab-org/gitlab/issues/28478#note_235108076)

- [x] Read the [materials for the Monitor Remote Offsite](https://gitlab.com/gitlab-org/monitor/general/issues/40#note_239837725)
 
- [x] Take the Git course on Front-end Masters
 
### Unplanned tasks
- [x] Review an MR
- [x] Provide feedback on Amelia's issues

### Most Important Meetings

(not including recurring company/stage group meetings)

- [x] 1-1 with Amelia

### Fun/Social Calls (aim for 1-2 per week)

- [x] Coffee chat with Michal
- [x] Coffee chat with Sunjung