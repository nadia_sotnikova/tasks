This file tracks my experiment in blocking off every Friday to focus on important and not urgent strategic work such as OKRs, personal career goals, etc. I'll document my experience and share it broadly to encourage others to dedicate more time to strategic work and take better control of their productive time.

On Focus Fridays I don't do meetings and only check email and Slack to ensure there's nothing urgent. If it can wait till Monday, I don't spend time on it.

My weekly planning documents contain a section called Focus Friday which I fill with items I'd like to focus on. I'll link these documents here in case you want to check out what kind of things I dedicate my time to on Fridays. 

- [Feb 12](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/2021-week-6.md#focus-fridays)
    - [Tweet about Focus Fridays](https://twitter.com/nadia_sotnikova/status/1360201668688809985)
