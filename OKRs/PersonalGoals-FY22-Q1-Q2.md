## 🌈🌻 Nadia's Personal Development Goals 🌻🌈

- `FY22-Q1: February 1 - April 30`
- `FY22-Q2: May 1 - July 31`

_Let's co-create something magical together!_ 🔮

### What is this

This document tracks my personal growth and career development strategy and deliverables for FY22-Q1 and FY22-Q2. 

- These goals are aligned with the [current UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1501) and the needs I see on the Verify:PA team, as well as my personal interests and inclinations.
- The goals align with the Product Designer role responsibilities, focusing on exemplifying the responsibilities of the [Sr. Product Designer role.](https://docs.google.com/spreadsheets/d/1831kFeiTtzqRhsq37wjUv80dVVUSv3YsQYlgx9ExrK0/edit#gid=395345394)

### The Goal

Grow as a Product Designer through executing an effective UX strategy and process at the Verify:Pipeline Authoring stage, and develop the necessary skills and experience towards a Senior Product Designer role.

## 💗 [FY22-Q1 FOCUS](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/29) 💗

## FY22-Q1-Q2 GOALS

#### Stage: Help Verify:Pipeline Authoring team validate its product and UX direction through a proactive UX strategy

**Key Results:**

- [x] [Identify opportunities to influence GitLab low SUS score at Pipeline Authoring](https://gitlab.com/gitlab-org/gitlab/-/issues/321278)
    - [x] Document and analyze SUS score results that relate to Pipeline Authoring
    - [x] Identify opportunities for improvement
    - [x] Create follow-up issues
- [ ] Document the missing [Pipeline Authoring JTBDs](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-authoring/jtdb/#jtbd)
    - [ ] Validate the JTBDs as necessary
- [ ] Apply UXDoD to design issues for 1 quarter, run a retrospective, consider using UXDoD in our UX process
- [ ] Organise PA research initiatives [see CI example](https://gitlab.com/groups/gitlab-org/-/epics/4927#note_448315299) and share it broadly
- [ ] Work with Dov to communicate PA product and UX direction broadly
    - https://gitlab.slack.com/archives/C0NFPSFA8/p1613055170044700
- [ ] Create a walkthrough video for the major epics together with Dov and share broadly.
- [ ] Participate in monthly kick-offs to present the UX direction
- [ ] Share the insights and other artifacts broadly
- [ ] Rescore the Pipeline Authoring experience by conducting a UX Scorecard and/ or CMS
- [ ] Document the Verify:Pipeline Authoring UX startegy in the Handbook Verify:Pipeline Authoring UX page
    - [ ] Update the UX CI/CD, UX Verify, and Verify Pipeline Authoring pages with a link to the PA UX page.

#### UX Dept: Contribute to increasing GitLab SUS Score by burning down Verify:Pipeline Authoring UX debt and UI polish issues

**Key Results:**

- [x] Identify Verify:Pipeline Authoring UX debt and UI polish issues
    - [x] Ensure issues have UI polish and UX debt labels
    - [x] Create [an epic to hold UX debt and UI polish issues](https://gitlab.com/groups/gitlab-org/-/epics/5389)
    - [x] Create dashboards to track PA UX debt and UI polish
    - [ ] Share in Verify channel, UX CI/CD, PA - share the cross-department OKR and ask to keep an eye out for UI polish issues, and add them to the epic 
- [ ] Work with PM to prioritize those issues, aim to schedule at least one UX debt/ UI polish issue per milestone
- [ ] Share the effort, the results, the retro broadly

#### Cross-stage: Contribute to higher Verify Stage adoption rate through close collaboration with the Growth UX and Product on the Pipeline Authoring part of Verify onboarding 

[See product performance KPIs](https://about.gitlab.com/handbook/product/performance-indicators#new-group-namespace-verify-stage-adoption-rate)

**Key Results**

- [ ] [Run regular meetings with Growth UX and Product to align on existing efforts and brainstorm around new opportunities](https://docs.google.com/document/d/18eN7HOeMeIntabrdX1gF9f-Dfm1k-U-QhOsl4icsW0E/edit?usp=sharing)
- [x] Schedule a transition meeting with Kevin and Emily to discuss Growth + Verify UX

#### Cross-stage: Foster cross-stage collaboration within the CI/CD UX team and their cross-functional team members

**Key Results:**

- [x] Conduct a survey on UX cross-stage collaboration to learn more about the problems we're facing. Share the insights widely.
- [x] Present at UX Showcase about cross-stage collaboration to bring explore the problems, the successes, and share the CI/CD experience with cross-stage collaboration.
- [ ] Set up coffee chats with PDs from all CI/CD and adjacent to CI/CD stages to learn more about their current UX and product direction, the challenges and opportunities they see. Share Verify:PA vision with them. Share recordings broadly.
- [ ] Set up coffee chats with Product Managers of all CI/CD and adjacent stages to learn more about their current product strategy, the challenges or opportunities they see. Share Verify:PA vision with them.
- [ ] Share the recordinngs or notes broadly. Maybe create an epic to track this, with cross-stage issues created based on the opportunities?
- [x] Share broadly [an example of collaboration with the Developer Evangelism team on gathering new feature feedback](https://gitlab.slack.com/archives/C03MSG8B7/p1613055687466900)

#### UX Dept: Mentor other designers and lead by example by sharing my work output, sharing my career growth, personal development, and mental health/wellness process

**Key Results:**

- [ ] The Process video series: Share about my process for getting to a visual builder MVC with the team sketching exercise, Think Big, Think Small, and then design iteration and implementation of the MVC. - share in UX channel and Verify-PA channel.
- [x] The Process video series: Share my [wins and lessons weekly retro process.](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/2020-week-48.md#wins-and-lessons) - share in UX channel and Verify-PA channel. https://www.youtube.com/watch?v=9AXOY9A7hCA
- [x] [Provide feedback in UX co-working channel once a week](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/Key%20results/Provide-UX-feedback-weekly.md)
- [ ] [Block off Focus Fridays for personal development work and share this experience broadly](https://gitlab.com/nadia_sotnikova/tasks/-/blob/master/OKRs/Key%20results/Focus-Fridays.md)

#### UX Dept: Become a Pajamas Figma Maintainer so I can be recognized as a domain expert in Pajamas and mentor others on how to contribute to the library

**Key Results:**

- [ ] Have a meeting with Jeremy to discuss the expectations of the maintainer training
- [ ] Identify areas within CI/CD that need to be aligned with Pajamas and create issues for those. Get Foundations team involved in those discussions as needed. Work with PM to schedule those issues as necessary.
- [ ] Conduct at least 2 reviews on new Pajamas contributions
- [ ] Make 4 Pajamas contributions 
- [ ] Provide feedback on the UI Kit maintainer training.







