## Hey, I’m Nadia! 🌻

I'm a Product Designer passionate about creating functional, intuitive and aesthetically beautiful user interfaces. Currently, I design application performance and health monitoring tools at GitLab. 

#### My path to Product Design ✨

Since early childhood I wanted to be a designer, but I ended up going to a business school instead. You know how it goes, family… After many twists and turns I Googled my way into a design career, and it’s been an amazing ride ever since! I’m very proud of being a self-taught designer. It takes enormous dedication and hard work to pivot your career, learn something on your own, and get into the industry without any connections. I believe my hunger for knowledge and ability to quickly acquire new skills with 0 guidance still makes me a better designer. Shoutout to all my self-taught designers! You rock!

#### What kind of a designer am I? 🤔

* I used to work for a UX agency, which exposed me to a very wide variety of projects. 
* My most memorable project was working with IBM. Eventually, that experience led me to GitLab.
* My design process is 80% understanding the problem, and 20% creating a solution. 
* I love and have a great eye for visual design. Unfortunately I don’t get to do a lot of it at GitLab, but I try to create more visually complex and challenging designs in my free time.
* The world of coding opened up a whole new dimension in Product Design for me. I’m very excited about contributing to Pajamas and working closely with engineers as I’m learning to code myself. 

#### My roots 🌳

I’m originally from Belarus, however over the past 10 years I’ve lived in Russia for 2 years, in the US (NJ) for 4 years, and in Portugal, Hungary, Indonesia and Spain from 1 months to a year. I’ve been to ~30 countries. 

I always stumble a little when people ask me where I’m from. After moving so many times I’m not sure where’s home anymore. It’s sad and exciting at the same time. 

#### What gets me out of bed in the morning 🌞

My favorite things to do are learning about this world, acquiring new skills and making things. It’s an added bonus if I can do it with a community of like-minded people. I spend a lot of time at co-working spaces doing just that. 

Some of the things you can find me doing when I’m not designing GitLab:

* Meditating and doing yoga
* Sweating profusely in a crossfit or a spin class
* Painting
* DIY-ing beautiful things for my home
* Reading books about design, science, psychology, philosophy or extraordinary people
* Cooking pad thais or mexican cuisine with my SO (who works remotely and travels with me)
* Having long and weird conversations with friends
* Traveling to new places
* Attending hackathons
* Taking courses and learning new things


#### How I work 💪

I’m a huge believer in deep work. Nothing meaningful can be created without flow. All of my work practices are aimed towards maximizing deep work and minimizing any distractions (meetings, I’m looking at you 👀 ). 

* I try to attend the least amount of meetings possible. 
* I put in a lot of effort into my written communication to minimize the need for synchronous meetings.
* I use a Pomodoro timer to help me stay focused on one task at a time.
* I spend a lot of time learning new things and increasing my efficiency, so I can accomplish more in less time.


#### Fun fact 😀

I’ve only worked in an office once, back in college, and it sucked. I’ve been working remotely since 2015, straight out of university. Not sure I’d survive in a co-located office environment. 



