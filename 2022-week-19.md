# Week 19 | May 16 - May 20 👁💗🤝✨

#### Main focus 

#### Milestone 

- [15.0](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/44#scope-of-work-for-ux)
- [15.1](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/55)

## Tasks

#### Research

- [x] Plan [UXR involvement in Q2](https://gitlab.com/gitlab-org/ux-research/-/issues/1875#gold-first_place-projects)
- [x] Review [Erika's report](https://gitlab.com/gitlab-org/ux-research/-/issues/1948)
- [x] Plan the [Pipeline Components MVC solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1942)
- [x] Create a screener
- [x] Finish the analysis for raw variables [solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1896) 
- [x] Update the FE issue with recs
- [x] Formulate the new open questions for variables research based on the results
- [x] Share the results with the PA, CI/CD UX and UX teams with a bullet point summary
- [ ] Create issues for the [ideas from the research](https://gitlab.com/gitlab-org/ux-research/-/issues/1948#note_948634456) and link to the Pipeline Catalog Epic. Ping Erika.

#### Design

- [x] Adjust the Catalog MVC designs and the [issue description](https://gitlab.com/gitlab-org/gitlab/-/issues/359047)
- [x] Create invites for the APAC/EMEA and the US meetings for next week, linking the video and issue in the event description and agenda

#### Pajamas

- [x] Utility sticky text update ([Figma branch](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/branch/wnc5x3SEy35JfUMIbtrupv/Component-library))
- [ ] Audit the [issues.svg illustration](https://gitlab.com/gitlab-org/gitlab/-/issues/358573)
- [x] Create a [draft MR](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2857) for new guidelines to the [Helping users](https://design.gitlab.com/usability/helping-users/#help-icon), [Drawers](https://design.gitlab.com/components/drawer) pages
- [ ] Contribute to the [WIP product illustrations style guide update in Figma](https://www.figma.com/file/1ui9w228X0S5WxaD0SRdIA/branch/tMPk4cKFcvn2A0GAp0M932/Illustration-library?node-id=0%3A1)

#### Other

#### Focus Friday 🔮 

🌻🌈 [FY23 Q2 CI/CD UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978#fy23-q2-okrs-for-cicd-ux) 🌻🌈 [FY23 Q2-Q3 Career Development](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [x] [Update Tanuki illustrations to use the new logo](https://gitlab.com/gitlab-org/gitlab/-/issues/356842)
- [x] Review [popover refactor](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1352#note_952121952)
- [ ] Refine the [Q2-3 Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) based on the feedback from Rayana
- [x] Pajamas traineeship checkin ([see here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835#note_929756938))
- [ ] Provide feedback in UX co-working 
- [x] Reflect on and plan next week

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>
- [ ] Share the final and shipped [guidelines for help drawer usage in UX channel](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2857/diffs#note_953730955) in the context of the options to help our users
- [ ] Refine the [Pipeline Authoring UX page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100082) and request a review from Rayana ([Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and [PE](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/) pages for structure)
- [ ] Add to the [PA 15.0 retro](https://gitlab.com/gl-retrospectives/verify-stage/pipeline-authoring/-/issues/23) and [UX 15.0 retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/54#note_941310545)
- [ ] Consume the [research for secrets](https://gitlab.com/gitlab-org/ux-research/-/issues/1873#note_918961390)


</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
