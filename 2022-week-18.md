# Week 18 | May 9 - May 13 👁💗🤝✨

#### Main focus 

- CI/CD catalog mockups and prototype
- UX Showcase

#### Milestone 

- [15.0](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/44#scope-of-work-for-ux)

## Tasks

#### Research

- [x] Plan [UXR involvement in Q2](https://gitlab.com/gitlab-org/ux-research/-/issues/1875#gold-first_place-projects)
- [x] Raw variables research analysis

#### Design

- [x] Refine the CI/CD Component Catalog mock-ups
- [x] Create a simple walkthrough deck for problem (personas and JTBDs and pain points), how  customers solve it today, proposed solution click-throhgh, next steps (refine mock-ups, run solution validation internally and externally, then hopefully dogfood)
- [-] Schedule a team discussion to gather feedback on the CI Catalog MVC from the whole PA team for when Dov is back

#### Pajamas

- [-] Audit the [issues.svg illustration](https://gitlab.com/gitlab-org/gitlab/-/issues/358573)
- [-] Create a draft MR for new guidelines to the [Helping users](https://design.gitlab.com/usability/helping-users/#help-icon), [Drawers](https://design.gitlab.com/components/drawer) pages
- [-] Contribute to the [WIP product illustrations style guide update in Figma](https://www.figma.com/file/1ui9w228X0S5WxaD0SRdIA/branch/tMPk4cKFcvn2A0GAp0M932/Illustration-library?node-id=0%3A1)

#### Other

- [x] Prep for UX Showcase based on the Pipeline Components deck
- [x] Work on a plan for the next quarter, including Q2 OKRs, personal career development and working on my developing skills as a Senior PD
- [x] [Plan 15.1](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/55#scope-of-work-for-ux)
- [x] 15.1 kick-off recording

#### Focus Friday 🔮 

🌻🌈 [FY23 Q2 CI/CD UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978#fy23-q2-okrs-for-cicd-ux) 🌻🌈 [FY23 Q2-Q3 Career Development](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈

- [-] Refine the [Q2-3 Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63)
- [-] Pajamas traineeship checkin ([see here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835#note_929756938))
- [x] Provide feedback in UX co-working 
- [x] Reflect on and plan next week

## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Adjust the Catalog MVC designs and the [issue description](https://gitlab.com/gitlab-org/gitlab/-/issues/359047)
- [ ] Record a 5-min walkthrough, including the next steps about the meetings and solution validation
- [ ] Create invites for the APAC/EMEA and the US meetings for next week, linking the video and issue in the event description and agenda
- [ ] Finish the analysis for raw variables [solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1896) 
- [ ] Update the FE issue with recs
- [ ] Formulate the new open questions for variables research based on the results
- [ ] Record a walkthrough of insights and recommendations
- [ ] Share the results with the PA, CI/CD UX and UX teams with a link to video + bullet point summary
- [ ] Consume the [research for secrets](https://gitlab.com/gitlab-org/ux-research/-/issues/1873#note_918961390)
- [ ] Refine the [Pipeline Authoring UX page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100082) and request a review from Rayana ([Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and [PE](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/) pages for structure)
- [ ] Add to the [PA 15.0 retro](https://gitlab.com/gl-retrospectives/verify-stage/pipeline-authoring/-/issues/23) and [UX 15.0 retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/54#note_941310545)

</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
