# Product Designer's cheat sheet for making Code Changes

## Using the GDK

### Checking out an MR locally

When checking out an MR, you will want to ensure the changes are pulled to your local:

* gdk update
* gdk start
* cd gitlab
* git fetch
* git checkout “branch-name”
* git reset --hard origin/branch-name
* gdk restart 

When switching to a new branch, it's often a good idea to run `gdk restart rails`

### Troubleshooting errros

```
/usr/local/Homebrew/Library/Homebrew/shims/mac/super/pkg-config --cflags  -- icu-i18n icu-i18n
/usr/local/Homebrew/Library/Homebrew/shims/mac/super/pkg-config: line 6: /pkg-config/bin/pkg-config: No such file or directory
/usr/local/Homebrew/Library/Homebrew/shims/mac/super/pkg-config: line 6: exec: /pkg-config/bin/pkg-config: cannot execute: No such file or directory
/usr/local/Homebrew/Library/Homebrew/shims/mac/super/pkg-config: exit status 126
make[1]: [build] Error 2
make: [gitlab-elasticsearch-indexer/bin/gitlab-elasticsearch-indexer] Error 2
```

Check the output of echo `$HOMEBREW_OPT` in your terminal. If it returns empty, try `export HOMEBREW_OPT="/usr/local/opt/"`, then run `gdk reconfigure` again and the errors should no longer be present.
If you open the file `/usr/local/Homebrew/Library/Homebrew/shims/mac/super/pkg-config`, you’ll see that it prefixes the `$HOMEBREW_OPT` path to  `/pkg-config/bin/pkg-config`. I’m not sure how or why this has changed in some way by updating go or gdk, but that does seem to be the problem.
If for some reason that fix doesn’t work for you, run `export HOMEBREW_OPT=""` to reset the change you made.

## Making code changes

### Changing text

1.  The first step is usually to search for the exact string in the /gitlab folder to figure out where that code lives. You can do it by searching for keywords or the exact string in the VSCode.

2.  If it's a text-only change, there might be a couple of different file types popping up:
- .js (except for app.js) or .vue files, this is where the change needs to be made
- app.js files in a /locale folder or any test/coverage files, don't worry about these
- gitlab.pot and multiple versions of that in different localizations, you will have to recreate these after making the changes by running `bin/rake gettext:regenerate` in the terminal

### Submitting an MR

#### Creating a changelog for user facing changes

Most user facing changes will require you to submit a change log. To create a change log, perform these steps after committing your changes:
*  run `bin/changelog "Description of the change here."`
*  Specify the category of the change by typing the appropriate number in the terminal.
```
>> Please specify the index for the category of your change:
1. New feature
2. Bug fix
3. Feature change
4. New deprecation
5. Feature removal
6. Security fix
7. Performance improvement
8. Other
```
*  Open the file that's been created in your text editor (the file path is returned to you in the terminal) and add your MR number and the author name (you) to that file.
*  Save
*  Git add, git commit, git push.

#### How to `git rebase`

You can use git rebase to edit the commits or squash them.

- `git rebase -i master`
- press i for insert
- delete ’pick` for the ones you want to edit
and reword or r instead
- hit esc and then type :x
- then you will be shown the messages and you can edit them. Change them and then hit esc and then type :x again to save.
That would rebase everything and reword the messaged. Alternatively you can squash the commits with the messages that you want to edit and simply type s or squash when replacing pick

### Video tutorials

- [How to git rebase -i master](https://www.loom.com/share/c7d64134f5564aa1ba1e26d10b3fd23b) 

*  [A tutorial for making a CSS change, checking it out locally, making an MR and submitting a changelog](https://www.youtube.com/watch?v=SSo97VwVn4Y&t=917s)