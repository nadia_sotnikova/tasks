# Week 24 | June 13 - June 15  👁💗🤝✨

#### Main focus 

- User interviews

#### Milestone 

- [15.1](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/55)

## Tasks

#### Research

- [x] Pipeline components MVC user interviews
- [x] Schedule 1 more interview for next week

#### Design

- [x] Catch up on all issue pings

#### Pajamas

- [x] Pajamas traineeship checkin, uncommit from things, reduce the scope, set timeline ([see here](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835#note_929756938))

#### Other

- [x] [15.2 planning](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/61#note_984314826)
- [x] Document the steps for reviewing saas FF MRs based on Emily's messages (make an MR to the UX MR review guidelines section)
- [x] Create an issue to update the pipeline editor drawer
- [x] Create a section for PA MR reviews [Veethika's example](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/#merge-request-reviews)  (see [the MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/106075))
- [x] Reflect on and plan next week

#### Focus Friday 🔮 

🌻🌈 [FY23 Q2 CI/CD UX OKRs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1978#fy23-q2-okrs-for-cicd-ux) 🌻🌈 [FY23 Q2-Q3 Career Development](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) 🌻🌈


## Wins and Lessons

<details><summary>Expand</summary>
<p>

#### Wins of the week 

- [Provided feedback to Gina on a Pajamas MR](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2901#note_996941093)
- Reviewed a gitlab-ui drawer MR and created a [follow-up issue](https://gitlab.com/gitlab-org/gitlab-ui/-/issues/1859) to define the footer usage guidelines

#### What I learnt 

#### What didn't go so well? Why?

</p>
</details>

## Next Week

<details><summary>Expand</summary>
<p>

- [ ] Update [this issue proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/217309#note_991832036)
- [ ] Put research and other important issues into the [PA roadmap](https://app.mural.co/t/gitlab2474/m/gitlab2474/1654692090314/fb8ffd76fbd73b4a15f00eeef9dff0d8c5b8175b?sender=dhershkovitch7730)
- [ ] [Self-assessment for the mid-year check-in](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2002)
- [ ] Refine the [Q2-3 Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/63) based on the feedback from Rayana and scope down ruthlessly
- [ ] Refine the [Pipeline Authoring UX page](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/100082) with info on merge request reviews for PA and request a review from Rayana ([Runner](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/runner/) and [PE](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/) pages for structure)


</details>

## Plan for later

<details><summary>Expand</summary>
<p>

- [ ] Update my ReadMe & share 

</p>
</details>
