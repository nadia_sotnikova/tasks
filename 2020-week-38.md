# Week 38 | September 14 - 18 ️✨🚀

#### Main focus 

- 13.5 UX planning
- OKR MRs
- 3 year Verify vision

#### Milestone 

13.4

## Tasks

#### Design

- [x] Groom DAG research follow-up issues and write an update to kick-off the discussion around the direction with CI visualization and issue prioritization.
- [x] 13.5 scope of UX planning with Dov and Jason
- [x] Work on [3 year vision](https://app.mural.co/t/gitlab2474/m/gitlab2474/1597220470608/bf10c92ce52729d900fedcb5114d8d97f1e5c519)
- [x] Add a table chart legend variant to Pajamas Data Viz (in progress)
- [ ] Audit for inline elements, work on a proposal https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/680#note_409277308

#### Other

- [x] Move forward the OKR MRs
   - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42077 (MERGED 💪)
   - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42009
- [x] Open 2 more MRs
   - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42215
   - https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42221 
   
- [x] Review https://gitlab.com/gitlab-org/verify-and-release-ux-team/-/issues/18 

#### Learning and Development

- [ ] [Pajamas Design System maintainer training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8835)

## Wins and Lessons

#### Wins of the week

- Made good progress with the 3 year vision flows, learnt a lot about the product in the process, though turned out I didn't need to dig deep outside of Pipeline Authoring. 
- Communicated the UX research findings around the DAG experience to the rest of the team and got on the same page around the Ci visualization direction.
- [Merged one OKR MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/42077) and opened and moved forward several others!
- Participated in discussions on several issues that require more UX consideration. For example, we're discussing the nuances of the CI config alerts throughout the UI https://gitlab.com/gitlab-org/gitlab/-/issues/251088#note_415007097
- Started working on [adding a tabular chart legend variant to Pajamas](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/715) (trying to slowly work on Pajamas maintainer training)

#### What I learnt

- Learnt how to git rebase and to edit commits. Also, learnt about component tests, where they're written, and more or less how they work. It was a frustrating but very rewarding journey. 💪

#### What did go so well? Why?

- It was not well communicated who should be working on what for the Verify 3 year vision. On the UX team right off the bat Veethika worked on a flow that spanned different teams, and we collaborated together on everything. That plus the issue gave me impression that we're supposed to collaborate together on a joint Verify vision. 

That miscommunication led to me doing extra work and being frustrated why I'm the only one trying to move this forward. Which was completely normal since I'm actually the one who should work with my PM only on the Pipeline Authoring part. 😂😂 

In retrospect, I could've asked for specific expectations on what should be delivered, what is the scope and purpose. However, as a team we should do a better job documenting all issues and all work async in a way that communicated the puspose and the DRIs for each part of the project/ task. I assumed that the issue where we were discussing it was correct, but it wasn't updated.

-----------

## Next Week

- [ ] Start discussions to understand the problem and define requirements for 13.5 issues
- [ ] Work on 3 year vision with Dov

## Current OKRs to focus on

#### UX

- [UX Q3 OKRs](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8363)
- [UX CI/CD issue to track all OKR issues](https://gitlab.com/gitlab-org/verify-and-release-ux-team/-/issues/13)

## Plan for later

- [ ] The career development Mural exercise. 
- [ ] Make a case for UI Polish and UX debt on our team, collect UX research evidence that affects NPS score
- [ ] Summarize the docs card sort process following [Justin's template](https://docs.google.com/document/d/1_Nx1wBZ7zTFRCVNKMMJfNywjdAYQIdkY5ALjMK7kbzw/edit#heading=h.gq9xxzqffb0h) (see [issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1213))
- [ ] Find some [weird error messages](https://gitlab.com/groups/gitlab-org/-/epics/3327) to fix