# Week 10 | March 1 - March 6

Main focus: Finalise 12.9 design issues

## Milestone

12.9

## Tasks

- [x] [Prometheus Research](https://gitlab.com/gitlab-org/ux-research/issues/553) [Affinity Mapping](https://app.mural.co/t/gitlab2474/m/gitlab2474/1582890127893/b7a348cccdff7dd61c5ef5d5c7d6b71a1d0c3940)
- [x] Create an Epic and UXR Insights for [Prometheus Research](https://gitlab.com/gitlab-org/ux-research/issues/553)
- [x] Finalise the design proposal for [Design issue: Add single timestamp to the log explorer time picker](https://gitlab.com/gitlab-org/gitlab/issues/201747)
- [x] Finalise the [menu keyboard shortcuts issues](https://gitlab.com/gitlab-org/gitlab/issues/202146)
- [x] Create an IA structure for the [monitoring docs, think how to set up a tree test.](https://gitlab.com/gitlab-org/gitlab/issues/199420)

## Learning and Development

- [ ] Complete 2 modules of Accessibility Course
- [ ] Write the blog post

## Admin

- [ ] Plan vacation time for 2020
- [ ] 12.10 planning

### Unplanned tasks

